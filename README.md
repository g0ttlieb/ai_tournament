# COMP3608 Advanced AI Tournament

## Context
This is my submission for the USYD Advanced AI tournament, designing an algorithm to most efficiently play Connect 4. The specs of the assignment can be found in `specs.pdf`, with this dealing with only the tournament component.

## Algorithm
A minimax algorithm is used with an evaluation function that best estimates the state of the game. It considers the winning 7 shape as highly optimal and then takes into account all possible chains that can still be completed, jumping gaps in them to do so and giving points the longer they are. Arguable most importantly, it also considers timing out and skipping a turn as an option since (for some reason) it is allowed in this tournament. This particularly takes advantage over algorithms that don't consider this. The numbers of levels considered is capped to ensure the time limit of 1 second allowed for each move is never exceeded.

## Results
Overall, this algorithm performed very well! I won the tournament with a scoreline of 65-3-2 wins-ties-losses in the double round-robin. This gave me a score of 198, 19 points ahead of second place!

## Disclaimer
While I think the overall style of the code is sorta fine, looking back at it there are tonnes of places where I would have done things differently. In particular, this was the moment when I swore to never use `i` or `j` in for-loops longer than 1 line! The score calculation is a mess and should definetely have been split into smaller functions. This can be said for a lot of things, but it is more egregious there!
