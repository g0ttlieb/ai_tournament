#include "state.hpp"
#include <iostream>
#include <limits>

// Constructor - from other state
State::State(State* parent, Node* node){

    // Copy over parents state (changed depth)
    depth = parent->getDepth() - 1;
    turn = parent->getTurn();
    board = parent->getBoard();

    // Propogate alpha-beta values of parent
    alpha = parent->getAlpha();
    beta = parent->getBeta();
    if(turn == Side::Red) alpha = std::max(alpha, parent->getVal());
    else beta = std::min(beta, parent->getVal());

    // Place tile for parent and change side
    placedNode = node;
    if(placedNode != nullptr) placedNode->side = turn;
    if(turn == Side::Red) turn = Side::Yellow;
    else turn = Side::Red;

    // Set starting val based on side
    if(turn == Side::Red) val = std::numeric_limits<int>::min();
    else val = std::numeric_limits<int>::max();
    col = -1;

}

// Constructor - from base info
State::State(Board* board, Side turn, int maxDepth){
    // Default vals
    depth = maxDepth;
    this->turn = turn;
    this->board = board;
    placedNode = nullptr;
    alpha = std::numeric_limits<int>::min();
    beta = std::numeric_limits<int>::max();
    // Set starting val based on side
    if(turn == Side::Red) val = std::numeric_limits<int>::min();
    else val = std::numeric_limits<int>::max();
    col = -1;
}

// Destructor - reset changed node
State::~State(){
    if(placedNode != nullptr)
        placedNode->side = Side::None;
}

// Process state
void State::processState(){

    // If at final depth - calc eval
    if(depth == 0){
        board->updateScore();
        val = board->getEval();
        return;
    }

    // Check not game over
    board->checkGameOver();
    if(board->isWon()){
        val = board->getUtility();
        return;
    }

    // Process children
    for(int icol = 0; icol < 8; icol++){
        // Skip filled cols
        Node* changeNode = board->getTopFree(icol);
        if(changeNode == nullptr && icol!=7) continue;
        // Generate new state and recurse
        State* newState = new State(this, changeNode);
        newState->processState();
        // Take best for current side
        if(turn == Side::Red){
            if(newState->getVal() > val){
                val = newState->getVal();
                col = icol;
            }
        }
        else{
            if(newState->getVal() < val){
                val = newState->getVal();
                col = icol;
            }
        }
        // Revert change made creating that state
        delete newState;
        // Prune alpha-beta
        if(turn == Side::Red && val >= beta)
            break;
        else if(turn == Side::Yellow && val <= alpha)
            break;
    }

}

// Getters
int State::getVal(){return val;}
int State::getCol(){return col;}
int State::getDepth(){return depth;}
Board* State::getBoard(){return board;}
Side State::getTurn(){return turn;}
int State::getAlpha(){return alpha;}
int State::getBeta(){return beta;}
