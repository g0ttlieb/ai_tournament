#ifndef BOARD_HPP
#define BOARD_HPP

#include <vector>
#include <string>

// Enum for each side
enum class Side{Red, Yellow, None};

// Nodes within board
class Node{
    public:
        // Constructor
        Node(char colour);
        // Fields
        Side side;
        std::vector<Node*> adj;
};

class Board{
    public:
        // Constructor / Destructor
        Board(std::string state);
        ~Board();
        // Methods
        Node* getTopFree(int col);
        void updateScore();
        void checkGameOver();
        // Getters
        int getUtility();
        int getEval();
        bool isWon();
        bool isBottomMiddleFree();
    private:
        // Helper methods
        int score(Side side);
        void setWinner(Side side);
        // Attributes
        bool gameOver;
        int util;
        int eval;
        Node*** board;
};

#endif
