#ifndef STATE_HPP
#define STATE_HPP

#include "board.hpp"

class State{
    public:
        // Constructor / Destructor
        State(State* parent, Node* node);
        State(Board* board, Side turn, int maxDepth);
        ~State();
        // Methods
        void processState();
        // Getters
        int getVal();
        int getCol();
        int getDepth();
        Board* getBoard();
        Side getTurn();
        int getAlpha();
        int getBeta();
    private:
        // Attributes
        int val;
        int col;
        int depth;
        Side turn;
        Board* board;
        Node* placedNode;
        int alpha;
        int beta;
};

#endif
