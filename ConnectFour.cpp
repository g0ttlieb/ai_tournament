#include <chrono>
#include <thread>
#include <iostream>
#include <string>
#include "board.hpp"
#include "state.hpp"

#define DEPTH 7

// Main!
int main(int argc, char** argv){

    // Read args
    std::string stateArg = argv[1];
    std::string turnArg = argv[2];

    // Parse basic args
    Side turn = Side::Red;
    if(turnArg != "red")
        turn = Side::Yellow;
    int maxDepth = DEPTH;

    // Create board / state and process
    Board* board = new Board(stateArg);
    State* state = new State(board, turn, maxDepth);
    state->processState();

    // Print output / timeout as required
    int colChoice = state->getCol();
    if(colChoice == 7)
        std::this_thread::sleep_for(std::chrono::seconds(2));
    else if(board->isBottomMiddleFree())
        std::cout << 3 << std::endl;
    else
        std::cout << colChoice << std::endl;

    // Cleanup
    delete board;
    delete state;

}
