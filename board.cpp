#include "board.hpp"
#include <iostream>

// Node constructor
Node::Node(char colour){
    if(colour == 'y') side = Side::Yellow;
    else if(colour == 'r') side = Side::Red;
    else side = Side::None;
    for(int i = 0; i < 8; i++)
        adj.push_back(nullptr);
}

// Board constructor
Board::Board(std::string state){

    // Init nodes
    int strIndex;
    board = new Node**[6];
    for(int row = 0; row < 6; row++){
        board[row] = new Node*[7];
        for(int col = 0; col < 7; col++){
            strIndex = ((5-row)*8) + col;
            board[row][col] = new Node(state.at(strIndex));
        }
    }

    // Setup adjacency pointers
    int newRow;
    int newCol;
    int adjIndex;
    for(int row = 0; row < 6; row++){
        for(int col = 0; col < 7; col++){
            Node* current = board[row][col];
            // Iterate over neighbours if valid pos
            for(int i = -1; i <= 1; i++){
                newRow = row + i;
                if(newRow < 0 || newRow >= 6) continue;
                for(int j = -1; j <= 1; j++){
                    if(i == 0 && j == 0) continue;
                    newCol = col + j;
                    if(newCol < 0 || newCol >= 7) continue;
                    // Set new pointer - skip center
                    adjIndex = (i+1)*3 + (j+1);
                    if(adjIndex >= 4) adjIndex--;
                    current->adj[adjIndex] = board[newRow][newCol];
                }
            }
        }
    }

}

// Board destructor
Board::~Board(){
    for(int i = 0; i < 6; i++){
        for(int j = 0; j < 7; j++)
            delete board[i][j];
        delete[] board[i];
    }
    delete[] board;
}

// Get top free node
Node* Board::getTopFree(int col){
    if(col > 6) return nullptr;
    for(int i = 5; i >= 0; i--){
        if(board[i][col]->side == Side::None)
            return board[i][col];
    }
    return nullptr;
}

// Update util / eval scores
void Board::updateScore(){
    checkGameOver();
    if(gameOver)
        eval = util;
    else{
        int redScore = score(Side::Red);
        int yellowScore = score(Side::Yellow);
        eval = redScore - yellowScore;
    }
}

// Get score for given side
int Board::score(Side side){

    // Init
    int scores[] = {0,0,0,0};
    Node* curNode;
    Node* prevNode;
    Node* searchNode;
    int curCount;
    int oppositeDir;
    int remainingCount;
    int jumpCount;
    Side otherSide = Side::Red;
    if(side == Side::Red) otherSide = Side::Yellow;

    // Iterate to find how many of each
    for(int row = 0; row < 6; row++){
        for(int col = 0; col < 7; col++){

            // Process current if correct side
            curNode = board[row][col];
            if(curNode->side != side) continue;
            scores[0]++;

            // Check all directions
            int curScores[] = {0,0,0,0,0,0,0,0};
            for(int dir = 0; dir < 8; dir++){
                // Skip upwards - no chance of anything new
                if(dir == 1) continue;
                // Get prev node in that direction and ensure None
                oppositeDir = 7-dir;
                prevNode = curNode->adj[oppositeDir];
                if(prevNode == nullptr || prevNode->side != Side::None) continue;
                // Search in that dir till end of path
                curCount = 0;
                searchNode = curNode;
                while(searchNode != nullptr && searchNode->side == side){
                    curCount++;
                    searchNode = searchNode->adj[dir];
                }
                // Make sure not an irrelevant chain - can get 4
                remainingCount = 4 - curCount;
                while(remainingCount > 0 && searchNode != nullptr &&
                        searchNode->side != otherSide){
                    remainingCount--;
                    searchNode = searchNode->adj[dir];
                }
                searchNode = prevNode;
                while(remainingCount > 0 && searchNode != nullptr &&
                        searchNode->side != otherSide){
                    remainingCount--;
                    searchNode = searchNode->adj[oppositeDir];
                }
                if(remainingCount > 0) continue;
                // Jump over None gap to find split 3
                jumpCount = curCount;
                if(dir >= 4 && curCount < 3){
                    searchNode = prevNode->adj[oppositeDir];
                    while(searchNode != nullptr && searchNode->side == side){
                        jumpCount++;
                        searchNode = searchNode->adj[oppositeDir];
                    }
                    if(jumpCount >= 3)
                        curCount = 3;
                }
                // Add size to count if actually makes chain
                // Will double count some chains (dir < 4 and ends in None)
                //   but this desired - 2 possible extensions so rewarded as such
                curScores[dir] = curCount;
                if(curCount > 1)
                    scores[curCount-1]++;
            }

            // Check for 7 shapes
            if(curScores[3] == 3 && (curScores[0] == 3 || curScores[5] == 3))
                scores[3]++;
            if(curScores[4] == 3 && (curScores[2] == 3 || curScores[7] == 3))
                scores[3]++;

        }
    }

    // Calc final score
    int finalScore = scores[0] + 10*scores[1] + 200*scores[2] + 5000*scores[3];
    return finalScore;
}

// Check for game over state
void Board::checkGameOver(){

    // Init
    gameOver = false;
    Side curSide;
    Node* curNode;
    Node* prevNode;
    Node* searchNode;
    int curCount;
    int oppositeDir;

    // Iterate to find how many of each
    for(int row = 0; row < 6; row++){
        for(int col = 0; col < 7; col++){

            // Process current if not empty
            curNode = board[row][col];
            curSide = curNode->side;
            if(curSide == Side::None) continue;

            // Check forward directions
            for(int dir = 4; dir < 8; dir++){
                // Get prev node in that direction and skip if same
                oppositeDir = 7-dir;
                prevNode = curNode->adj[oppositeDir];
                if(prevNode != nullptr && prevNode->side == curSide) continue;
                // Search in that dir till end of path
                curCount = 0;
                searchNode = curNode;
                while(searchNode != nullptr && searchNode->side == curSide){
                    curCount++;
                    searchNode = searchNode->adj[dir];
                }
                if(curCount >= 4){
                    setWinner(curSide);
                    return;
                }
            }
        }
    }
}



// Set winner as given side
void Board::setWinner(Side side){
    gameOver = true;
    if(side == Side::Red)
        util = 100000;
    else
        util = -100000;
}

// Getters
int Board::getEval(){return eval;}
int Board::getUtility(){
    if(gameOver) return util;
    else return 0;
}
bool Board::isWon(){return gameOver;}
bool Board::isBottomMiddleFree(){
    return board[4][3]->side == Side::None;
}
